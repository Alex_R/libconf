#ifndef CONF_HELPER_H_INCLUDED
#define CONF_HELPER_H_INCLUDED

#include <stdbool.h>


#define CONF_HELPER_WHITESPACE_CHARS      " \f\t\v"
#define CONF_HELPER_NEWLINE_CHARS         "\n\r"
#define CONF_HELPER_QUOTE_CHARS           "\""
#define CONF_HELPER_ESCAPER_CHARS         "\\"
#define CONF_HELPER_BLOCK_START_CHARS     "{"
#define CONF_HELPER_BLOCK_END_CHARS       "}"
#define CONF_HELPER_TOKEN_END_CHARS       ";,\n"
#define CONF_HELPER_TOKEN_SEPERATOR_CHARS ":"
#define CONF_HELPER_DIGIT_CHARS           "0123456789"
#define CONF_HELPER_SIGN_CHARS            "+-"

#define CONF_HELPER_ONELINE_COMMENT_START_SEQ   "//"
#define CONF_HELPER_MULTILINE_COMMENT_START_SEQ "/*"
#define CONF_HELPER_MULTILINE_COMMENT_END_SEQ   "*/"

#define CONF_HELPER_BOOL_FALSE_SEQ "false"
#define CONF_HELPER_BOOL_TRUE_SEQ  "true"


/* conf_helper_is_whitespace
 * Check if the supplied character is whitespace.
 * See CONF_HELPER_WHITESPACE_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is whitespace, False otherwise.
 */
bool conf_helper_is_whitespace(char character);

/* conf_helper_is_newline
 * Check if the supplied character is a newline.
 * See CONF_HELPER_NEWLINE_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is whitespace, False otherwise.
 */
bool conf_helper_is_newline(char character);

/* conf_helper_is_quote
 * Check if the supplied character is a quote.
 * See CONF_HELPER_QUOTE_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is a quote, False otherwise.
 */
bool conf_helper_is_quote(char character);

/* conf_helper_is_escaper
 * Check if the supplied character is an "escaper" (a character that escapes things in strings).
 * See CONF_HELPER_ESCAPER_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is a quote, False otherwise.
 */
bool conf_helper_is_escaper(char character);

/* conf_helper_is_block_start
 * Check if the supplied character is a block start.
 * See CONF_HELPER_BLOCK_START_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is a block start, False otherwise.
 */
bool conf_helper_is_block_start(char character);

/* conf_helper_is_block_end
 * Check if the supplied character is a block end.
 * See CONF_HELPER_BLOCK_END_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is a block end, False otherwise.
 */
bool conf_helper_is_block_end(char character);

/* conf_helper_is_token_end
 * Check if the supplied character is a token end.
 * See CONF_HELPER_TOKEN_END_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is a token end, False otherwise.
 */
bool conf_helper_is_token_end(char character);

/* conf_helper_is_token_seperator
 * Check if the supplied character is a token seperator.
 * See CONF_HELPER_TOKEN_SEPERATOR_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is a token seperator, False otherwise.
 */
bool conf_helper_is_token_seperator(char character);

/* conf_helper_is_digit
 * Check if the supplied character is a digit.
 * See CONF_HELPER_DIGIT_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is a digit, False otherwise.
 */
bool conf_helper_is_digit(char character);

/* conf_helper_is_sign
 * Check if the supplied character is a sign.
 * See CONF_HELPER_SIGN_CHARS.
 *
 * @ char character - The character to check.
 *
 * # bool - True if the character is a sign, False otherwise.
 */
bool conf_helper_is_sign(char character);


/* conf_helper_is_oneline_comment_start
 * Check if the supplied characters are the start of a oneline comment.
 * See CONF_HELPER_ONELINE_COMMENT_START_SEQ.
 *
 * @ const char *characters - The characters to check.
 *
 * # bool - True if the characters are the start of a oneline comment, False otherwise.
 */
bool conf_helper_is_oneline_comment_start(const char *characters);

/* conf_helper_is_multiline_comment_start
 * Check if the supplied characters are the start of a multiline comment.
 * See CONF_HELPER_MULTILINE_COMMENT_START_SEQ.
 *
 * @ const char *characters - The characters to check.
 *
 * # bool - True if the characters are the start of a multiline comment, False otherwise.
 */
bool conf_helper_is_multiline_comment_start(const char *characters);

/* conf_helper_is_multiline_comment_end
 * Check if the supplied characters are the end of a multiline comment.
 * See CONF_HELPER_MULTILINE_COMMENT_END_SEQ.
 *
 * @ const char *characters - The characters to check.
 *
 * # bool - True if the characters are the end of a multiline comment, False otherwise.
 */
bool conf_helper_is_multiline_comment_end(const char *characters);

/* conf_helper_is_bool_false
 * Check if the supplied characters represent a false bool.
 * See CONF_HELPER_BOOL_FALSE_SEQ.
 *
 * @ const char *characters - The characters to check.
 *
 * # bool - True if the characters represent a false bool, False otherwise.
 */
bool conf_helper_is_bool_false(const char *characters);

/* conf_helper_is_bool_true
 * Check if the supplied characters represent a true bool.
 * See CONF_HELPER_BOOL_TRUE_SEQ.
 *
 * @ const char *characters - The characters to check.
 *
 * # bool - True if the characters represent a true bool, False otherwise.
 */
bool conf_helper_is_bool_true(const char *characters);


/* conf_helper_is_escaped
 * Check if the supplied character is escaped with slashes.
 * This scans backwards from the supplied character_ptr, at most reverse_len characters.
 * See conf_helper_is_escaper().
 *
 * @ const char *character_ptr - The position of the character in the string to check for escapedness.
 * @ size_t reverse_len        - The "reverse length" of the string (between the character_ptr and the start).
 *
 * # bool - True if the character is escaped, False otherwise.
 */
bool conf_helper_is_escaped(const char *character_ptr, size_t reverse_len);


/* conf_helper_update_in_string
 * Update the supplied in_string variable using the supplied current and start positions.
 *
 * @ bool *in_string        - The in_string bool to update.
 * @ const char *curr       - The current position in the data.
 * @ const char *data_start - The start position in the data.
 */
void conf_helper_update_in_string(bool *in_string, const char *curr, const char *data_start);


/* conf_helper_unescape_string
 * Unescape the supplied string.  The string is modified in place.
 *
 * @ char *string - The string to unescape.
 */
void conf_helper_unescape_string(char *string);

#endif
