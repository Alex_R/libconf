#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#include "conf_value.h"
#include "conf.h"
#include "conf_helper.h"
#include "conf_error.h"


static char *_conf_value_parse_str(struct conf *conf, const char *string_data) {
    /* _conf_value_parse_str
     * Parse the supplied string data.
     *
     * @ struct conf *conf       - The parent conf.
     * @ const char *string_data - The string data to parse.
     *
     * # char * - A newly allocated string representing the parsed string data, or NULL on error with
     *            conf->error set appropriately.
     */

    char *str_value;

    str_value = strdup(&string_data[1]);
    if(str_value == NULL) {
        CONF_ERROR_SET(conf, "_conf_value_parse_str('%s') failed: strdup() failed: %s", string_data,
                                                                                           strerror(errno));
        return NULL;
    }

    str_value[strlen(str_value) - 1] = '\0';

    conf_helper_unescape_string(str_value);

    return str_value;
}


conf_int_t _conf_value_parse_int_abs(conf_int_t val) {
    return (val < 0) ? (0 - val) : val;
}

conf_int_t _conf_value_parse_int_pow(conf_int_t base, conf_int_t exp) {
    /* _conf_value_parse_int_pow
     * Return the result of pow(base, exp).
     * Note: Shamelessly stolen from the internet.
     */

    conf_int_t result = 1;

    while(exp) {
        if(exp & 1) {
            result *= base;
        }

        exp >>= 1;
        base *= base;
    }

    return result;
}


static conf_int_t *_conf_value_parse_int(struct conf *conf, const char *int_data) {
    /* _conf_value_parse_int
     * Parse the supplied int data.
     *
     * @ struct conf *conf    - The parent conf.
     * @ const char *int_data - The int data to parse.
     *
     * # conf_int_t * - A newly allocated int value, or NULL on error with conf->error set appropriately.
     */

    conf_int_t *v_int, v_int_base, v_int_exp;
    char bin[8];

    v_int = malloc(sizeof(conf_int_t));
    if(v_int == NULL) {
        CONF_ERROR_SET(conf, "_conf_value_parse_int(%s) failed: malloc() failed: %s", int_data, strerror(errno));
        return NULL;
    }

    /* Scientific notation. */
    if(              sscanf(int_data, "%"CONF_INT_T_SCN"%[eE]%"CONF_INT_T_SCN, &v_int_base, bin, &v_int_exp) == 3) {
        *v_int = (v_int_base * _conf_value_parse_int_pow(10, _conf_value_parse_int_abs(v_int_exp)));
        return v_int;

    /* Power notation. */
    } else if(         sscanf(int_data, "%"CONF_INT_T_SCN"^%"CONF_INT_T_SCN, &v_int_base, &v_int_exp) == 2) {
        *v_int = _conf_value_parse_int_pow(v_int_base, _conf_value_parse_int_abs(v_int_exp));
        return v_int;

    /* Octal. */
    } else if(       sscanf(int_data, "0%[oO]%"CONF_INT_T_SCN_OCT, bin, v_int) == 2) {
        return v_int;

    /* Hex. */
    } else if(       sscanf(int_data, "0%[xX]%"CONF_INT_T_SCN_HEX, bin, v_int) == 2) {
        return v_int;

    /* Standard digits. */
    } else if(       sscanf(int_data, "%"CONF_INT_T_SCN, v_int) == 1) {
        return v_int;

    /* No matches. */
    } else {
        CONF_ERROR_SET(conf, "_conf_value_parse_int(%s) failed: Value doesn't match any formats", int_data);
        return NULL;
    }
}


static bool *_conf_value_parse_bool(struct conf *conf, const char *bool_data) {
    /* _conf_value_parse_bool
     * Parse the supplied bool data.
     *
     * @ struct conf *conf     - The parent conf.
     * @ const char *bool_data - The bool data to parse.
     *
     * # bool * - A newly allocated bool value, or NULL on error with conf->error set appropriately.
     */

    bool *v_bool;

    v_bool = malloc(sizeof(bool));
    if(v_bool == NULL) {
        CONF_ERROR_SET(conf, "_conf_value_parse_bool(%s) failed: malloc() failed: %s", bool_data, strerror(errno));
        return NULL;
    }

    if(       conf_helper_is_bool_false(bool_data)) {
        *v_bool = false;
    } else if(conf_helper_is_bool_true(bool_data)) {
        *v_bool = true;
    } else {
        CONF_ERROR_SET(conf, "_conf_value_parse_bool(%s) failed: Invalid value for bool", bool_data);
        return NULL;
    }

    return v_bool;
}



struct conf_value *conf_value_create(struct conf *conf, const char *value_name, const char *value_data) {
    struct conf_value *value;
    conf_int_t *v_int;
    bool *v_bool;

    value = malloc(sizeof(struct conf_value));
    if(value == NULL) {
        CONF_ERROR_SET(conf, "conf_value_create() failed: malloc() failed: %s", strerror(errno));
        return NULL;
    }
    memset(value, 0, sizeof(struct conf_value));

    value->name = strdup(value_name);
    if(value->name == NULL) {
        CONF_ERROR_SET(conf, "conf_value_create() failed: strdup() failed: %s", strerror(errno));
        free(value);
        return NULL;
    }

    if(       conf_helper_is_quote(value_data[0])) {
        value->type = CONF_VALUE_TYPE_STR;

        value->v_str = _conf_value_parse_str(conf, value_data);
        if(value->v_str == NULL) {
            free(value);
            return NULL;
        }
    } else if(conf_helper_is_bool_false(value_data) || conf_helper_is_bool_true(value_data)) {
        value->type = CONF_VALUE_TYPE_BOOL;

        v_bool = _conf_value_parse_bool(conf, value_data);
        if(v_bool == NULL) {
            free(value);
            return NULL;
        }

        value->v_bool = *v_bool;
        free(v_bool);
    } else if(conf_helper_is_digit(value_data[0]) || conf_helper_is_sign(value_data[0])) {
        value->type = CONF_VALUE_TYPE_INT;

        v_int = _conf_value_parse_int(conf, value_data);
        if(v_int == NULL) {
            free(value);
            return NULL;
        }

        value->v_int = *v_int;
        free(v_int);
    } else {
        CONF_ERROR_SET(conf, "conf_value_create(%s, %s) failed: Unrecognised type", value_name, value_data);
        free(value);
        return NULL;
    }

    return value;
}

void conf_value_destroy(struct conf_value *value) {
    if(value->type == CONF_VALUE_TYPE_STR) {
        free(value->v_str);
    }

    free(value->name);
    free(value);
}
