#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>

#include "conf_error.h"
#include "conf.h"


struct conf_error *conf_error_create(const char *format, ...) {
    struct conf_error *error;
    va_list vargs;
    int value_len;

    error = malloc(sizeof(struct conf_error));
    if(error == NULL) {
        return NULL;
    }
    memset(error, 0, sizeof(struct conf_error));

    va_start(vargs, format);

    value_len = vsnprintf(NULL, 0, format, vargs);
    if(value_len < 0) {
        free(error);
        return NULL;
    }

    va_end(vargs);
    va_start(vargs, format);

    error->value = malloc(sizeof(char) * value_len);
    if(error->value == NULL) {
        free(error);
        return NULL;
    }

    if(vsnprintf(error->value, value_len, format, vargs) < 0) {
        free(error->value);
        free(error);
        return NULL;
    }

    va_end(vargs);

    return error;
}

void conf_error_destroy(struct conf_error *error) {
    free(error->value);
    free(error);
}
