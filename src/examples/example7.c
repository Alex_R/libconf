#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "conf.h"


#define EXAMPLE_CONFIG_FILE "./example_config.conf"


static void _grab_error_callback(struct conf *conf, const char *error) {
    fprintf(stderr, "_grab_error_callback(%p, '%s')\n", conf, error);
    exit(EXIT_FAILURE);
}


int main(int argc, char *argv[]) {
    struct conf *conf;
    struct conf_block *block;
    
    struct main_config_s {
        struct {
            char       *sub_str_value;
            conf_int_t sub_int_value;
            bool       sub_bool_value;
        } a_block;
    } main_config;

    printf("conf_create()\n");
    conf = conf_create();
    if(conf == NULL) {
        fprintf(stderr, "conf_create() failed: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    printf("conf_load_file(%p, '%s'\n", conf, EXAMPLE_CONFIG_FILE);
    if(conf_load_file(conf, EXAMPLE_CONFIG_FILE) == -1) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    if(conf_grab_set_error_callback(conf, _grab_error_callback) == -1) {
        fprintf(stderr, "conf_grab_set_error_callback() failed: %s\n", conf_get_error(conf));
        return EXIT_FAILURE;
    }

    conf_grab_block_nodef(conf, &block, "a_block");

    conf_grab_value_str_rel_nodef(conf, block, &main_config.a_block.sub_str_value, "sub_str_value");
    conf_grab_value_int_rel_nodef(conf, block, &main_config.a_block.sub_int_value, "sub_int_value");
    conf_grab_value_bool_rel_nodef(conf, block, &main_config.a_block.sub_bool_value, "sub_bool_value");

    printf("a_block:\n");
    printf("  sub_str_value:  '%s'\n", main_config.a_block.sub_str_value);
    printf("  sub_int_value:  %"CONF_INT_T_PRI"\n", main_config.a_block.sub_int_value);
    printf("  sub_bool_value: %s\n", conf_get_bool_str(main_config.a_block.sub_bool_value));

    printf("conf_destroy(%p)\n", conf);
    conf_destroy(conf);

    return EXIT_SUCCESS;
}
