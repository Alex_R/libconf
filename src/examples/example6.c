#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "conf.h"


#define EXAMPLE_CONFIG_FILE "./example_config.conf"


static void _grab_error_callback(struct conf *conf, const char *error) {
    fprintf(stderr, "_grab_error_callback(%p, '%s')\n", conf, error);
    exit(EXIT_FAILURE);
}


int main(int argc, char *argv[]) {
    struct conf *conf;

    struct main_config_s {
        char *log_level1;
        char *log_level2;
        char *log_level3;
    } main_config;

    conf_int_t log_level1_int;
    conf_int_t log_level2_int;
    conf_int_t log_level3_int;

    struct conf_str_int_map log_level_map[] = {
        {"info",  1},
        {"warn",  2},
        {"error", 3},

        {NULL, 0},
    };

    printf("conf_create()\n");
    conf = conf_create();
    if(conf == NULL) {
        fprintf(stderr, "conf_create() failed: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    printf("conf_load_file(%p, '%s'\n", conf, EXAMPLE_CONFIG_FILE);
    if(conf_load_file(conf, EXAMPLE_CONFIG_FILE) == -1) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    if(conf_grab_set_error_callback(conf, _grab_error_callback) == -1) {
        fprintf(stderr, "conf_grab_set_error_callback() failed: %s\n", conf_get_error(conf));
        return EXIT_FAILURE;
    }

    conf_magic_grab_value_str(conf, &main_config, log_level1, NULL);
    conf_magic_grab_value_str(conf, &main_config, log_level2, NULL);
    conf_magic_grab_value_str(conf, &main_config, log_level3, NULL);

    log_level1_int = conf_map_str_to_int(main_config.log_level1, log_level_map, -1);
    log_level2_int = conf_map_str_to_int(main_config.log_level2, log_level_map, -1);
    log_level3_int = conf_map_str_to_int(main_config.log_level3, log_level_map, -1);

    printf("log_level1: '%s' %"CONF_INT_T_PRI"\n", main_config.log_level1, log_level1_int);
    printf("log_level2: '%s' %"CONF_INT_T_PRI"\n", main_config.log_level2, log_level2_int);
    printf("log_level3: '%s' %"CONF_INT_T_PRI"\n", main_config.log_level3, log_level3_int);

    free(main_config.log_level1);
    free(main_config.log_level2);
    free(main_config.log_level3);

    printf("conf_destroy(%p)\n", conf);
    conf_destroy(conf);

    return EXIT_SUCCESS;
}
