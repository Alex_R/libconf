#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "conf.h"


#define EXAMPLE_CONFIG_FILE "./example_config.conf"


int main(int argc, char *argv[]) {
    struct conf *conf;
    struct conf_value *value;
    char *str_value;
    conf_int_t *int_value;
    bool *bool_value;
    struct conf_block *block;

    printf("conf_create()\n");
    conf = conf_create();
    if(conf == NULL) {
        fprintf(stderr, "conf_create() failed: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    printf("conf_load_file(%p, '%s')\n", conf, EXAMPLE_CONFIG_FILE);
    if(conf_load_file(conf, EXAMPLE_CONFIG_FILE) == -1) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("conf_get_value(%p, NULL, 'str_value')\n", conf);
    value = conf_get_value(conf, NULL, "str_value");
    if(value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("conf_get_value_str(%p)\n", value);
    str_value = conf_get_value_str(value);
    if(str_value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("str_value: '%s'\n", str_value);

    printf("conf_get_value(%p, NULL, 'int_value')\n", conf);
    value = conf_get_value(conf, NULL, "int_value");
    if(value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("conf_get_value_int(%p)\n", value);
    int_value = conf_get_value_int(value);
    if(int_value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("int_value: %"CONF_INT_T_PRI"\n", *int_value);

    printf("conf_get_value(%p, NULL, 'bool_value')\n", conf);
    value = conf_get_value(conf, NULL, "bool_value");
    if(value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("conf_get_value_bool(%p)\n", value);
    bool_value = conf_get_value_bool(value);
    if(bool_value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("bool_value: %s\n", conf_get_bool_str(*bool_value));

    printf("conf_get_block(%p, NULL, 'a_block')\n", conf);
    block = conf_get_block(conf, NULL, "a_block");
    if(block == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("a_block: %p\n", block);

    printf("conf_get_value(%p, %p, 'sub_str_value')\n", conf, block);
    value = conf_get_value(conf, block, "sub_str_value");
    if(value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("conf_get_value_str(%p)\n", value);
    str_value = conf_get_value_str(value);
    if(str_value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("sub_str_value: '%s'\n", str_value);

    printf("conf_get_value(%p, %p, 'sub_int_value')\n", conf, block);
    value = conf_get_value(conf, block, "sub_int_value");
    if(value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("conf_get_value_int(%p)\n", value);
    int_value = conf_get_value_int(value);
    if(int_value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("sub_int_value: %"CONF_INT_T_PRI"\n", *int_value);

    printf("conf_get_value(%p, %p, 'sub_bool_value')\n", conf, block);
    value = conf_get_value(conf, block, "sub_bool_value");
    if(value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("conf_get_value_bool(%p)\n", value);
    bool_value = conf_get_value_bool(value);
    if(bool_value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("sub_bool_value: %s\n", conf_get_bool_str(*bool_value));

    printf("conf_get_value(%p, NULL, 'str_value_with_escapes')\n", conf);
    value = conf_get_value(conf, NULL, "str_value_with_escapes");
    if(value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    str_value = conf_get_value_str(value);
    if(str_value == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("str_value_with_escapes: '%s'\n", str_value);

    printf("conf_destroy(%p)\n", conf);
    conf_destroy(conf);

    return EXIT_SUCCESS;
}
