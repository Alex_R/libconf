#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "conf.h"


#define EXAMPLE_CONFIG_FILE "./example_config.conf"


int main(int argc, char *argv[]) {
    struct conf *conf;
    char *str_value;
    conf_int_t int_value;
    bool bool_value;
    struct conf_block *block;

    printf("conf_create()\n");
    conf = conf_create();
    if(conf == NULL) {
        fprintf(stderr, "conf_create() failed: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    printf("conf_load_file(%p, '%s')\n", conf, EXAMPLE_CONFIG_FILE);
    if(conf_load_file(conf, EXAMPLE_CONFIG_FILE) == -1) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("conf_get_value_str_or(%p, NULL, 'str_value', NULL)\n", conf);
    str_value = conf_get_value_str_or(conf, NULL, "str_value", NULL);
    printf("str_value: '%s'\n", str_value);

    printf("conf_get_value_int_or(%p, NULL, 'int_value', -1)\n", conf);
    int_value = conf_get_value_int_or(conf, NULL, "int_value", -1);
    printf("int_value: %"CONF_INT_T_PRI"\n", int_value);

    printf("conf_get_value_bool_or(%p, NULL, 'bool_value', false)\n", conf);
    bool_value = conf_get_value_bool_or(conf, NULL, "bool_value", false);
    printf("bool_value: %s\n", conf_get_bool_str(bool_value));

    printf("conf_get_block(%p, NULL, 'a_block')\n", conf);
    block = conf_get_block(conf, NULL, "a_block");
    if(block == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("a_block: %p\n", block);

    printf("conf_get_value_str_or(%p, %p, 'sub_str_value', NULL)\n", conf, block);
    str_value = conf_get_value_str_or(conf, block, "sub_str_value", NULL);
    printf("sub_str_value: '%s'\n", str_value);

    printf("conf_get_value_int_or(%p, %p, 'sub_int_value', -1)\n", conf, block);
    int_value = conf_get_value_int_or(conf, block, "sub_int_value", -1);
    printf("sub_int_value: %"CONF_INT_T_PRI"\n", int_value);

    printf("conf_get_value_bool_or(%p, %p, 'sub_bool_value', false)\n", conf, block);
    bool_value = conf_get_value_bool_or(conf, block, "sub_bool_value", false);
    printf("sub_bool_value: %s\n", conf_get_bool_str(bool_value));

    printf("conf_get_value_str_or(%p, NULL, 'str_value_with_escapes', NULL)\n", conf);
    str_value = conf_get_value_str_or(conf, NULL, "str_value_with_escapes", NULL);
    printf("str_value_with_escapes: '%s'\n", str_value);

    printf("conf_destroy(%p)\n", conf);
    conf_destroy(conf);

    return EXIT_SUCCESS;
}
