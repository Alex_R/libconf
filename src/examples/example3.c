#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "conf.h"


#define EXAMPLE_CONFIG_FILE "./example_config.conf"


int main(int argc, char *argv[]) {
    struct conf *conf;
    struct conf_iter *iter;
    struct conf_value *value;
    struct conf_block *block;
    conf_int_t index;

    printf("conf_create()\n");
    conf = conf_create();
    if(conf == NULL) {
        fprintf(stderr, "conf_create() failed: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    printf("conf_load_file(%p, '%s')\n", conf, EXAMPLE_CONFIG_FILE);
    if(conf_load_file(conf, EXAMPLE_CONFIG_FILE) == -1) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("conf_iter_create(%p, NULL)\n", conf);
    iter = conf_iter_create(conf, NULL);
    if(iter == NULL) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    printf("blocks:\n");
    index = 0;
    while((block = conf_iter_next_block(iter)) != NULL) {
        printf("  %"CONF_INT_T_PRI": %s, %p\n", index, conf_get_block_name(block), block);

        index++;
    }

    printf("values:\n");
    index = 0;
    while((value = conf_iter_next_value(iter)) != NULL) {
        printf("  %"CONF_INT_T_PRI": %s, ", index, conf_get_value_name(value));

        if(       conf_get_value_type(value) == CONF_VALUE_TYPE_STR) {
            printf("%s\n", conf_get_value_str(value));
        } else if(conf_get_value_type(value) == CONF_VALUE_TYPE_INT) {
            printf("%"CONF_INT_T_PRI"\n", *conf_get_value_int(value));
        } else if(conf_get_value_type(value) == CONF_VALUE_TYPE_BOOL) {
            printf("%s\n", conf_get_bool_str(*conf_get_value_bool(value)));
        }

        index++;
    }

    printf("conf_iter_destroy(%p)\n", iter);
    conf_iter_destroy(iter);

    printf("conf_destroy(%p)\n", conf);
    conf_destroy(conf);

    return EXIT_SUCCESS;
}
