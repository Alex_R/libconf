#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "conf.h"


#define EXAMPLE_CONFIG_FILE "./example_config.conf"


static void _grab_error_callback(struct conf *conf, const char *error) {
    fprintf(stderr, "_grab_error_callback(%p, '%s')\n", conf, error);
    exit(EXIT_FAILURE);
}


int main(int argc, char *argv[]) {
    struct conf *conf;

    struct main_config_s {
        char       *str_value;
        conf_int_t int_value;
        bool       bool_value;

        struct {
            char       *sub_str_value;
            conf_int_t sub_int_value;
            bool       sub_bool_value;
        } a_block;

        char *str_value_with_escapes;

        conf_int_t normal_int;
        conf_int_t hex_int;
        conf_int_t octal_int;
        conf_int_t power_int;
        conf_int_t scientific_int;
    } main_config;

    printf("conf_create()\n");
    conf = conf_create();
    if(conf == NULL) {
        fprintf(stderr, "conf_create() failed: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    printf("conf_load_file(%p, '%s'\n", conf, EXAMPLE_CONFIG_FILE);
    if(conf_load_file(conf, EXAMPLE_CONFIG_FILE) == -1) {
        fprintf(stderr, "failed: %s\n", conf_get_error(conf));
        conf_destroy(conf);
        return EXIT_FAILURE;
    }

    if(conf_grab_set_error_callback(conf, _grab_error_callback) == -1) {
        fprintf(stderr, "conf_grab_set_error_callback() failed: %s\n", conf_get_error(conf));
        return EXIT_FAILURE;
    }

    conf_magic_grab_value_str( conf, &main_config, str_value);
    conf_magic_grab_value_int( conf, &main_config, int_value);
    conf_magic_grab_value_bool(conf, &main_config, bool_value);

    conf_magic_grab_value_str( conf, &main_config, a_block.sub_str_value);
    conf_magic_grab_value_int( conf, &main_config, a_block.sub_int_value);
    conf_magic_grab_value_bool(conf, &main_config, a_block.sub_bool_value);

    conf_magic_grab_value_str(conf, &main_config, str_value_with_escapes);

    conf_magic_grab_value_int(conf, &main_config, normal_int);
    conf_magic_grab_value_int(conf, &main_config, hex_int);
    conf_magic_grab_value_int(conf, &main_config, octal_int);
    conf_magic_grab_value_int(conf, &main_config, power_int);
    conf_magic_grab_value_int(conf, &main_config, scientific_int);

    printf("str_value:  '%s'\n", main_config.str_value);
    printf("int_value:  %"CONF_INT_T_PRI"\n", main_config.int_value);
    printf("bool_value: %s\n", conf_get_bool_str(main_config.bool_value));

    printf("a_block:\n");
    printf("  sub_str_value:  '%s'\n", main_config.a_block.sub_str_value);
    printf("  sub_int_value:  %"CONF_INT_T_PRI"\n", main_config.a_block.sub_int_value);
    printf("  sub_bool_value: %s\n", conf_get_bool_str(main_config.a_block.sub_bool_value));

    printf("str_value_with_escapes: '%s'\n", main_config.str_value_with_escapes);

    printf("normal_int:     %"CONF_INT_T_PRI"\n", main_config.normal_int);
    printf("hex_int:        %"CONF_INT_T_PRI"\n", main_config.hex_int);
    printf("octal_int:      %"CONF_INT_T_PRI"\n", main_config.octal_int);
    printf("power_int:      %"CONF_INT_T_PRI"\n", main_config.power_int);
    printf("scientific_int: %"CONF_INT_T_PRI"\n", main_config.scientific_int);

    free(main_config.str_value);
    free(main_config.a_block.sub_str_value);
    free(main_config.str_value_with_escapes);

    printf("conf_destroy(%p)\n", conf);
    conf_destroy(conf);

    return EXIT_SUCCESS;
}
