#ifndef CONF_ERROR_H_INCLUDED
#define CONF_ERROR_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "conf.h"


/* CONF_ERROR_SET
 * A thin wrapper for conf_error_create that abort()s on error.
 *
 * @ struct conf *conf  - The conf to set the error on.
 * @ const char *format - The format string of the error.
 * @ ...                - Extra arguments.
 */
#define CONF_ERROR_SET(conf, format, ...)                                                                        \
    if(conf->error != NULL) {                                                                                    \
        fprintf(stderr, "CONF_ERROR_SET(): Replacing previous error '%s'\n", conf->error->value);                \
        conf_error_destroy(conf->error);                                                                         \
    }                                                                                                            \
    conf->error = conf_error_create(format, __VA_ARGS__);                                                        \
    if(conf->error == NULL) {                                                                                    \
        fprintf(stderr, "CONF_ERROR_SET() failed: conf_error_create(%s) failed: %s\n", format, strerror(errno)); \
        abort();                                                                                                 \
    }


/* conf_error
 * Encapsulates a conf error.
 */
struct conf_error {
    char *value;
};


/* conf_error_create
 * Allocate and initialise a new conf_error structure representing the supplied values.
 *
 * @ const char *format - The format string of the error.
 * @ ...                - Extra arguments.
 *
 * # struct conf_error * - A newly allocated conf_error structure repsenting the data, or NULL on error
 *                         with errno set appropriately.  The errors that may occur are those for malloc(3), and
 *                         strdup(3).
 */
struct conf_error *conf_error_create(const char *format, ...);

/* conf_error_destroy
 * Destroy the supplied conf_error structure.
 *
 * @ struct conf_error *error - The error to destroy.
 */
void conf_error_destroy(struct conf_error *error);


/* conf_error_set
 * Create and set a new error on the supplied conf structure, using the supplied values.
 * Note: This function will abort on failure, rather than returning an error value.
 *
 * @ struct conf *conf - The conf structure to set the error on.
 * @ const char *value - The value of the error (e.g. the descriptive string).
 * @ const char *file  - The file the error occured in, if applicable.
 * @ int line          - The line the error occured on, if applicable.
 * @ int column        - The column the error occured on, if applicable.
 */
void conf_error_set(struct conf *conf, const char *value, const char *file, int line, int column);

#endif
