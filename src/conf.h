#ifndef CONF_H_INCLUDED
#define CONF_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>


/* The type used in all integer value operations.
 * int64_t should be used wherever available, but if compiling for a weird embedded system change this to
 * int32_t or whatever is appropriate.
 */
typedef int64_t conf_int_t;

/* The print/scan formatters to use when printing / scanning the above conf_int_t. */
#define CONF_INT_T_SCN SCNd64
#define CONF_INT_T_PRI PRId64

/* Extra scan formatters, used in conf_value. */
#define CONF_INT_T_SCN_HEX SCNx64
#define CONF_INT_T_SCN_OCT SCNo64


/* Aren't circular dependencies fun?  We need to prototype this here to avoid the includes below from
 * trying to define it in their function prototypes.
 */
struct conf;


#include "conf_file.h"
#include "conf_block.h"
#include "conf_value.h"
#include "conf_error.h"


typedef void (conf_grab_error_callback_t)(struct conf *conf, const char *error);


/* conf
 * Encapsulates a conf.
 */
struct conf {
    struct conf_file *file;
    struct conf_error *error;
    conf_grab_error_callback_t *grab_error_callback;
};


/* conf_iter
 * Encapsulates a conf iterator.
 */
struct conf_iter {
    struct conf_block *block;
    conf_int_t block_index, value_index;
};


/* conf_str_int_map
 * Describes a str to int mapping.
 */
struct conf_str_int_map {
    char *key;
    conf_int_t value;
};


/* conf_create
 * Allocate and initialise a new empty conf structure.
 *
 * # struct conf *conf - A newly allocated empty conf_structure, or NULL on error with errno set appropriately.
 *                       The errors that may occur are those for malloc(3).
 */
struct conf *conf_create(void);

/* conf_destroy
 * Destroy the supplied conf structure.
 *
 * @ struct conf *conf - The conf to destroy.
 */
void conf_destroy(struct conf *conf);


/* conf_get_error
 * Get the last error to occur within the context of the supplied conf structure.
 *
 * @ struct conf *conf - The conf structure to get the last error from.
 *
 * # const char * - A string representing the error, or NULL if none exists.
 */
const char *conf_get_error(struct conf *conf);


/* conf_load_file
 * Load the file at the supplied path into the supplied conf structure.
 *
 * @ struct conf *conf     - The conf structure to load the file into.
 * @ const char *file_path - The path to the file to load.
 *
 * int - 0 on success, or -1 on error with conf->error set appropriately.
 */
int conf_load_file(struct conf *conf, const char *file_path);


/* conf_get_block
 * Get a reference to the block with the supplied name in the supplied block.
 * Supply NULL as the block to get from the root block.
 *
 * @ struct conf *conf        - The parent conf.
 * @ struct conf_block *block - The conf block to get the block from.
 * @ const char *name         - The name of the block to get.
 *
 * # struct conf_block * - A reference to the block, or NULL if it doesn't exist.
 */
struct conf_block *conf_get_block(struct conf *conf, struct conf_block *block, const char *name);

/* conf_get_value
 * Get a reference to the value with the supplied name in the supplied block.
 * Supply NULL as the block to get from the root block.
 *
 * @ struct conf *conf        - The parent conf.
 * @ struct conf_block *block - The conf block to get the value from.
 * @ const char *name         - The name of the value to get.
 *
 * # struct conf_value * - A reference to the value, or NULL if it doesn't exist.
 */
struct conf_value *conf_get_value(struct conf *conf, struct conf_block *block, const char *name);


/* conf_get_value_str
 * Interpret the supplied value as a string.
 *
 * @ struct conf_value *value - The conf value to interpret as a string.
 *
 * # char * - The string value, or NULL if the value is not a string.
 */
char *conf_get_value_str(struct conf_value *value);

/* conf_get_value_int
 * Interpret the supplied value as an int.
 *
 * @ struct conf_value *value - The conf value to interpret as an int.
 *
 * # conf_int_t * - A pointer to the int value, or NULL if the value is not an int.
 */
conf_int_t *conf_get_value_int(struct conf_value *value);

/* conf_get_value_bool
 * Interpret the supplied value as a bool.
 *
 * @ struct conf_value *value - The conf value to interpret as a bool.
 *
 * # bool * - A pointer to the bool value, or NULL if the value is not a bool.
 */
bool *conf_get_value_bool(struct conf_value *value);


/* conf_get_value_str_or
 * Get the value with the supplied name and interpret it as a string.
 * If the value exists and is a string it is returned, otherwise the supplied
 * default value is returned.
 * Supply NULL as the block to get from the root block.
 *
 * @ struct conf *conf        - The parent conf.
 * @ struct conf_block *block - The block to get the value from.
 * @ const char *name         - The name of the value to get.
 * @ char *def                - The default value to return on failure.
 *
 * # char * - The string value.
 */
char *conf_get_value_str_or(struct conf *conf, struct conf_block *block, const char *name, char *def);

/* conf_get_value_int_or
 * Get the value with the supplied name and interpret it as an int.
 * If the value exists and is an int it is returned, otherwise the supplied
 * default value is returned.
 * Supply NULL as the block to get from the root block.
 *
 * @ struct conf *conf        - The parent conf.
 * @ struct conf_block *block - The block to get the value from.
 * @ const char *name         - The name of the value to get.
 * @ conf_int_t def           - The default value to return on failure.
 *
 * # conf_int_t - The int value.
 */
conf_int_t conf_get_value_int_or(struct conf *conf, struct conf_block *block, const char *name, conf_int_t def);

/* conf_get_value_bool_or
 * Get the value with the supplied name and interpret it as a bool.
 * If the value exists and is a bool it is returned, otherwise the supplied
 * default value is returned.
 * Supply NULL as the block to get from the root block.
 *
 * @ struct conf *conf        - The parent conf.
 * @ struct conf_block *block - The block to get the value from.
 * @ const char *name         - The name of the value to get.
 * @ bool def                 - The default value to return on failure.
 *
 * # bool - The bool value.
 */
bool conf_get_value_bool_or(struct conf *conf, struct conf_block *block, const char *name, bool def);


/* conf_grab_set_error_callback
 * Set the grab error callback function.
 * The function is called when the conf_grab_value_* functions encounter an error.
 * The string supplied to the callback function describes the error encountered.
 *
 * @ struct conf *conf                           - The parent conf.
 * @ conf_grab_error_callback_t * error_callback - The callback function.
 *
 * # int - 0 on success, -1 on error with conf->error set appropriately.
 */
int conf_grab_set_error_callback(struct conf *conf, conf_grab_error_callback_t *error_callback);

/* conf_grab_value_str
 * Grab the str value located at the supplied path into the supplied destination.
 * If the value exists and is a str it is grabbed, otherwise the supplied default
 * value is grabbed.
 * In either case (grab or default), the value is duplicated with strdup() before being
 * stored in the destination.  The caller is responsible for free()ing this value.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf - The parent conf.
 * @ char **dest       - The destination to grab the value into.
 * @ char *path        - The path describing the location of the value.
 * @ char *def         - The default value to grab on failure.
 */
void conf_grab_value_str(struct conf *conf, char **dest, char *path, char *def);

/* conf_grab_value_int
 * Grab the int value located at the supplied path into the supplied destination.
 * If the value exists and is an int it is grabbed, otherwise the supplied default
 * value is grabbed.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf - The parent conf.
 * @ conf_int_t *dest  - The destination to grab the value into.
 * @ char *path        - The path describing the location of the value.
 * @ conf_int_t def    - The default value to grab on failure.
 */
void conf_grab_value_int(struct conf *conf, conf_int_t *dest, char *path, conf_int_t def);

/* conf_grab_value_bool
 * Grab the bool value located at the supplied path into the supplied destination.
 * If the value exists and is an int it is grabbed, otherwise the supplied default
 * value is grabbed
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf - The parent conf.
 * @ bool *dest        - The destination to grab the value into.
 * @ char *path        - The path describing the location of the value.
 * @ bool def          - The default value to grab on failure.
 */
void conf_grab_value_bool(struct conf *conf, bool *dest, char *path, bool def);

/* conf_grab_block
 * Grab the block located at the supplied path into the supplied destination.
 * If the block exists and is a block it is grabbed, otherwise the supplied default
 * block is grabbed.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf        - The parent conf.
 * @ struct conf_block **dest - The destination to grab the block into.
 * @ char *path               - The path describing the location of the block.
 * @ struct conf_block *def   - The default block to grab on failure.
 */
void conf_grab_block(struct conf *conf, struct conf_block **dest, char *path, struct conf_block *def);

/* conf_grab_value_str_rel
 * Grab the str value located at the supplied path, relative to the supplied block, into the supplied destination.
 * Functions the same as conf_grab_value_str, but relative to the supplied block.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf       - The parent conf.
 * @ struct conf_block *root - The block to grab relative to.
 * @ char **dest             - The destination to grab the value into.
 * @ char *path              - The path describing the location of the value, relative to the block.
 * @ char *def               - The default value to grab on failure.
 */
void conf_grab_value_str_rel(struct conf *conf, struct conf_block *root, char **dest, char *path, char *def);

/* conf_grab_value_int_rel
 * Grab the int value located at the supplied path, relative to the supplied block, into the supplied destination.
 * Functions the same as conf_grab_value_int, but relative to the supplied block.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf       - The parent conf.
 * @ struct conf_block *root - The block to grab relative to.
 * @ conf_int_t *dest        - The destination to grab the value into.
 * @ char *path              - The path describing the location of the value, relative to the block.
 * @ conf_int_t def          - The default value to grab on failure.
 */
void conf_grab_value_int_rel(struct conf *conf, struct conf_block *root, conf_int_t *dest, char *path, conf_int_t def);

/* conf_grab_value_bool_rel
 * Grab the bool value located at the supplied path, relative to the supplied block, into the supplied destination.
 * Functions the same as conf_grab_value_bool, but relative to the supplied block.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf       - The parent conf.
 * @ struct conf_block *root - The block to grab relative to.
 * @ bool *dest              - The destination to grab the value into.
 * @ char *path              - The path describing the location of the value, relative to the block.
 * @ bool def                - The default value to grab on failure.
 */
void conf_grab_value_bool_rel(struct conf *conf, struct conf_block *root, bool *dest, char *path, bool def);

/* conf_grab_block_rel
 * Grab the block located at the supplied path, relative to the supplied block, into the supplied destination.
 * Functions the same as conf_grab_block, but relative to the supplied block.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf        - The parent conf.
 * @ struct conf_block *root  - The block to grab relative to.
 * @ struct conf_block **dest - The destination to grab the block into.
 * @ char *path               - The path describing the location of the block, relative to the block.
 * @ struct conf_block *def   - The default block to grab on failure.
 */
void conf_grab_block_rel(struct conf *conf, struct conf_block *root, struct conf_block **dest, char *path, struct conf_block *def);

/* conf_grab_value_str_nodef
 * Grab the str value located at the supplied path into the supplied destination.
 * Functions the same as conf_grab_value_str, but raises an error instead of using the default.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf - The parent conf.
 * @ char **dest       - The destination to grab the value into.
 * @ char *path        - The path describing the location of the value.
 */
void conf_grab_value_str_nodef(struct conf *conf, char **dest, char *path);

/* conf_grab_value_int_nodef
 * Grab the int value located at the supplied path into the supplied destination.
 * Functions the same as conf_grab_value_int, but raises an error instead of using the default.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf - The parent conf.
 * @ conf_int_t *dest  - The destination to grab the value into.
 * @ char *path        - The path describing the location of the value.
 */
void conf_grab_value_int_nodef(struct conf *conf, conf_int_t *dest, char *path);

/* conf_grab_value_bool_nodef
 * Grab the bool value located at the supplied path into the supplied destination.
 * Functions the same as conf_grab_value_bool, but raises an error instead of using the default.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf - The parent conf.
 * @ bool *dest        - The destination to grab the value into.
 * @ char *path        - The path describing the location of the value.
 */
void conf_grab_value_bool_nodef(struct conf *conf, bool *dest, char *path);

/* conf_grab_block_nodef
 * Grab the block located at the supplied path into the supplied destination.
 * Functions the same as conf_grab_block, but raises an error instead of using the default.
 * calls the grab error callback on error.
 *
 * @ struct conf *conf - The parent conf.
 * @ bool *dest        - The destination to grab the block into.
 * @ char *path        - The path describing the location of the block.
 */
void conf_grab_block_nodef(struct conf *conf, struct conf_block **dest, char *path);

/* conf_grab_value_str_rel_nodef
 * Grab the str value located at the supplied path, relative to the supplied block, into the supplied destination.
 * Functions the same as conf_grab_value_str_rel, but raises an error instead of using the default.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf       - The parent conf.
 * @ struct conf_block *root - The block to grab relative to.
 * @ char **dest             - The destination to grab the value into.
 * @ char *path              - The path describing the location of the value.
 */
void conf_grab_value_str_rel_nodef(struct conf *conf, struct conf_block *root, char **dest, char *path);

/* conf_grab_value_int_rel_nodef
 * Grab the int value located at the supplied path, relative to the supplied block, into the supplied destination.
 * Functions the same as conf_grab_value_int_rel, but raises an error instead of using the default.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf       - The parent conf.
 * @ struct conf_block *root - The block to grab relative to.
 * @ conf_int_t *dest        - The destination to grab the value into.
 * @ char *path              - The path describing the location of the value.
 */
void conf_grab_value_int_rel_nodef(struct conf *conf, struct conf_block *root, conf_int_t *dest, char *path);

/* conf_grab_value_bool_rel_nodef
 * Grab the bool value located at the supplied path, relative to the supplied block, into the supplied destination.
 * Functions the same as conf_grab_value_bool_rel, but raises an error instead of using the default.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf       - The parent conf.
 * @ struct conf_block *root - The block to grab relative to.
 * @ bool *dest              - The destination to grab the value into.
 * @ char *path              - The path describing the location of the value.
 */
void conf_grab_value_bool_rel_nodef(struct conf *conf, struct conf_block *root, bool *dest, char *path);

/* conf_grab_block_rel_nodef
 * Grab the block located at the supplied path, relative to the supplied block, into the supplied destination.
 * Functions the same as conf_grab_block_rel, but raises an error instead of using the default.
 * Calls the grab error callback on error.
 *
 * @ struct conf *conf        - The parent conf.
 * @ struct conf_block *root  - The block to grab relative to.
 * @ struct conf_block **dest - The destination to grab the block into.
 * @ char *path               - The path describing the location of the block.
 */
void conf_grab_block_rel_nodef(struct conf *conf, struct conf_block *root, struct conf_block **dest, char *path);


/* Warning!  Here be dragons (read: C preprocessor abuse).
 * These macros describe a "magic grab" interface to the above grab interface.
 * The main idea is to reduce the degree to which the library user has to repeat theirself when
 * using the grab interface.
 *
 * For example it allows:
 *     conf_grab_value_int(conf, &main_config.a_block.sub_int_value, "a_block.sub_int_value", 0);
 * To be written as:
 *     conf_magic_grab_value_int(conf, &main_config, a_block.sub_int_value, 0);
 * It also (optionally) allows the removal of the default (calling the _nodef grab variant instead):
 *     conf_magic_grab_value_int(conf, &main_config, a_block.sub_int_value);
 *
 * This is a big improvement (imho, and especially on large, deeply nested configs), but comes 
 * at the possible cost of obscure compiler warning/error messages and hard to debug problems
 * that have you crawling around this header file looking at the macros.
 *
 * Use at your own risk.  If in doubt just use the non-magic grab interface.
 */
#define _conf_magic_grab_overload_get_4(_1, _2, _3, _4,     NAME, ...) NAME
#define _conf_magic_grab_overload_get_5(_1, _2, _3, _4, _5, NAME, ...) NAME
#define _conf_magic_grab_not_enough_args(...) Not enough arguments passed to libconf magic grab interface.

#define _conf_magic_grab_value_str_1(conf, base, path, def) \
    conf_grab_value_str(conf, base.path, #path, def)
#define _conf_magic_grab_value_str_0(conf, base, path) \
    conf_grab_value_str_nodef(conf, base.path, #path);
#define conf_magic_grab_value_str(...)                                             \
    _conf_magic_grab_overload_get_4(__VA_ARGS__, _conf_magic_grab_value_str_1,     \
                                                 _conf_magic_grab_value_str_0,     \
                                                 _conf_magic_grab_not_enough_args, \
                                                 _conf_magic_grab_not_enough_args, \
                                   )(__VA_ARGS__)

#define _conf_magic_grab_value_int_1(conf, base, path, def) \
    conf_grab_value_int(conf, base.path, #path, def)
#define _conf_magic_grab_value_int_0(conf, base, path) \
    conf_grab_value_int_nodef(conf, base.path, #path)
#define conf_magic_grab_value_int(...)                                             \
    _conf_magic_grab_overload_get_4(__VA_ARGS__, _conf_magic_grab_value_int_1,     \
                                                 _conf_magic_grab_value_int_0,     \
                                                 _conf_magic_grab_not_enough_args, \
                                                 _conf_magic_grab_not_enough_args, \
                                   )(__VA_ARGS__)

#define _conf_magic_grab_value_bool_1(conf, base, path, def) \
    conf_grab_value_bool(conf, base.path, #path, def)
#define _conf_magic_grab_value_bool_0(conf, base, path) \
    conf_grab_value_bool_nodef(conf, base.path, #path)
#define conf_magic_grab_value_bool(...)                                             \
    _conf_magic_grab_overload_get_4(__VA_ARGS__, _conf_magic_grab_value_bool_1,     \
                                                 _conf_magic_grab_value_bool_0,     \
                                                 _conf_magic_grab_not_enough_args,  \
                                                 _conf_magic_grab_not_enough_args,  \
                                   )(__VA_ARGS__)

/* This is a small extension to the above macros which allow an additional header to be added
 * to the path before resolution.
 * For example:
 *   conf_magic_headed_grab_value_str(conf, "thing", &thing_config, sub_block.a_value)
 */
#define _conf_magic_headed_grab_value_str_1(conf, header, base, path, def) \
    conf_grab_value_str(conf, base.path, header "." #path, def)
#define _conf_magic_headed_grab_value_str_0(conf, header, base, path) \
    conf_grab_value_str_nodef(conf, base.path, header "." #path);
#define conf_magic_headed_grab_value_str(...)                                         \
    _conf_magic_grab_overload_get_5(__VA_ARGS__, _conf_magic_headed_grab_value_str_1, \
                                                 _conf_magic_headed_grab_value_str_0, \
                                                 _conf_magic_grab_not_enough_args,    \
                                                 _conf_magic_grab_not_enough_args,    \
                                                 _conf_magic_grab_not_enough_args,    \
                                   )(__VA_ARGS__)

#define _conf_magic_headed_grab_value_int_1(conf, header, base, path, def) \
    conf_grab_value_int(conf, base.path, header "." #path, def)
#define _conf_magic_headed_grab_value_int_0(conf, header, base, path) \
    conf_grab_value_int_nodef(conf, base.path, header "." #path);
#define conf_magic_headed_grab_value_int(...)                                         \
    _conf_magic_grab_overload_get_5(__VA_ARGS__, _conf_magic_headed_grab_value_int_1, \
                                                 _conf_magic_headed_grab_value_int_0, \
                                                 _conf_magic_grab_not_enough_args,    \
                                                 _conf_magic_grab_not_enough_args,    \
                                                 _conf_magic_grab_not_enough_args,    \
                                   )(__VA_ARGS__)

#define _conf_magic_headed_grab_value_bool_1(conf, header, base, path, def) \
    conf_grab_value_bool(conf, base.path, header "." #path, def)
#define _conf_magic_headed_grab_value_bool_0(conf, header, base, path) \
    conf_grab_value_bool_nodef(conf, base.path, header "." #path);
#define conf_magic_headed_grab_value_bool(...)                                         \
    _conf_magic_grab_overload_get_5(__VA_ARGS__, _conf_magic_headed_grab_value_bool_1, \
                                                 _conf_magic_headed_grab_value_bool_0, \
                                                 _conf_magic_grab_not_enough_args,     \
                                                 _conf_magic_grab_not_enough_args,     \
                                                 _conf_magic_grab_not_enough_args,     \
                                   )(__VA_ARGS__)


/* conf_get_bool_str
 * Get the string representation of the supplied bool.
 *
 * @ bool v_bool - The bool to get the string representation of.
 *
 * # const char * - The string representation of the bool.
 */
const char *conf_get_bool_str(bool v_bool);


/* conf_map_str_to_int
 * Map the supplied string to an integer, using the supplied map.
 * The supplied map must be a list of conf_str_int_map elements, ending in a NULL sentinal ({NULL, 0}).
 *
 * @ const char *str              - The string to map.
 * @ struct conf_str_int_map *map - The map to use.
 * @ conf_int_t def               - The default value to use if the string does not map.
 *
 * # conf_int_t - The mapped integer.
 */
conf_int_t conf_map_str_to_int(const char *str, struct conf_str_int_map *map, conf_int_t def);


/* conf_get_block_name
 * Get the name of the supplied block.
 *
 * @ struct conf_block *block - The block to get the name of.
 *
 * # char * - The name of the block.
 */
char *conf_get_block_name(struct conf_block *block);


/* conf_get_value_type
 * Get the type of the supplied value.
 * See the CONF_VALUE_TYPE_* constants.
 *
 * @ struct conf_value *value - The value to get the type of.
 *
 * # int - The type of the value.
 */
int conf_get_value_type(struct conf_value *value);

/* conf_get_value_name
 * Get the name of the supplied value.
 *
 * @ struct conf_value *value - The value to get the name of.
 *
 * # char * - The name of the value.
 */
char *conf_get_value_name(struct conf_value *value);


/* conf_iter_create
 * Allocate and initialise a new conf iterator for the supplied block.
 * Supply NULL as the block to iterate over the root block.
 * See conf_iter_next_block and conf_iter_next_value for iteration.
 *
 * @ struct conf *conf        - The parent conf.
 * @ struct conf_block *block - The block to create the iterator for.
 *
 * # struct conf_iter * - The newly created iterator, or NULL on error with conf->error set appropriately.
 */
struct conf_iter *conf_iter_create(struct conf *conf, struct conf_block *block);

/* conf_iter_destroy
 * Destroy the supplied conf iterator.
 *
 * @ struct conf_iter *iter - The iterator to destroy.
 */
void conf_iter_destroy(struct conf_iter *iter);

/* conf_iter_next_block
 * Get the next block for the supplied iterator.
 *
 * @ struct conf_iter *iter - The iterator to get the next block for.
 *
 * # struct conf_block * - The next block, or NULL if the iteration is finished.
 */
struct conf_block *conf_iter_next_block(struct conf_iter *iter);

/* conf_iter_next_value
 * Get the next value for the supplied iterator.
 *
 * @ struct conf_iter *iter - The iterator to get the next value for.
 *
 * # struct conf_value * - The next value, or NULL if the iteration is finished.
 */
struct conf_value *conf_iter_next_value(struct conf_iter *iter);

#endif
