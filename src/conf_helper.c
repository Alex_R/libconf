#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "conf_helper.h"


static bool _conf_helper_is_in_string(char character, const char *string) {
    /* _conf_helper_is_in_string
     * Check if the supplied character is in the supplied string.
     *
     * @ char character     - The character to check.
     * @ const char *string - The string to check in.
     *
     * bool - True if the character is in the string, False otherwise.
     */

    const char *curr;

    for(curr = string; (*curr) != '\0'; curr++) {
        if((*curr) == character) {
            return true;
        }
    }

    return false;
}


static bool _conf_helper_is_sequence(const char *characters, const char *sequence) {
    /* _conf_helper_is_sequence
     * Check if the supplied characters are the supplied sequence.
     *
     * @ const char *characters - The characters to check.
     * @ const char *sequence   - The sequence to check against.
     */

    if(strlen(characters) < strlen(sequence)) {
        return false;
    }

    return (strncmp(characters, sequence, strlen(sequence)) == 0) ? true : false;
}


bool conf_helper_is_whitespace(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_WHITESPACE_CHARS);
}

bool conf_helper_is_newline(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_NEWLINE_CHARS);
}

bool conf_helper_is_quote(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_QUOTE_CHARS);
}

bool conf_helper_is_escaper(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_ESCAPER_CHARS);
}

bool conf_helper_is_block_start(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_BLOCK_START_CHARS);
}

bool conf_helper_is_block_end(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_BLOCK_END_CHARS);
}

bool conf_helper_is_token_end(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_TOKEN_END_CHARS);
}

bool conf_helper_is_token_seperator(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_TOKEN_SEPERATOR_CHARS);
}

bool conf_helper_is_digit(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_DIGIT_CHARS);
}

bool conf_helper_is_sign(char character) {
    return _conf_helper_is_in_string(character, CONF_HELPER_SIGN_CHARS);
}


bool conf_helper_is_oneline_comment_start(const char *characters) {
    return _conf_helper_is_sequence(characters, CONF_HELPER_ONELINE_COMMENT_START_SEQ);
}

bool conf_helper_is_multiline_comment_start(const char *characters) {
    return _conf_helper_is_sequence(characters, CONF_HELPER_MULTILINE_COMMENT_START_SEQ);
}

bool conf_helper_is_multiline_comment_end(const char *characters) {
    return _conf_helper_is_sequence(characters, CONF_HELPER_MULTILINE_COMMENT_END_SEQ);
}

bool conf_helper_is_bool_false(const char *characters) {
    return _conf_helper_is_sequence(characters, CONF_HELPER_BOOL_FALSE_SEQ);
}

bool conf_helper_is_bool_true(const char *characters) {
    return _conf_helper_is_sequence(characters, CONF_HELPER_BOOL_TRUE_SEQ);
}


bool conf_helper_is_escaped(const char *character_ptr, size_t reverse_len) {
    const char *curr;
    int escapers;

    escapers = 0;
    for(curr = (character_ptr - 1); (character_ptr - curr) < reverse_len; curr--) {
        if(conf_helper_is_escaper(*curr)) {
            escapers++;
        } else {
            break;
        }
    }

    return ((escapers % 2) == 0) ? false : true;
}


void conf_helper_update_in_string(bool *in_string, const char *curr, const char *data_start) {
    if(conf_helper_is_quote(*curr)) {
        if(*in_string) {
            if(!conf_helper_is_escaped(curr, (curr - data_start))) {
                *in_string = false;
            }
        } else {
            *in_string = true;
        }
    }
}


static const char *_conf_helper_unescape_token(const char *token) {
    /* _conf_helper_unescape_token
     * Unescape the supplied token.
     *
     * @ char *token      - The token to unescape.
     *
     * @ const char * - The unescaped representation of the token, or NULL if there isn't one.
     */

    if(_conf_helper_is_sequence(token, "\\t")) {
        return "\t";
    }

    if(_conf_helper_is_sequence(token, "\\v")) {
        return "\v";
    }

    if(_conf_helper_is_sequence(token, "\\n")) {
        return "\n";
    }

    if(_conf_helper_is_sequence(token, "\\r")) {
        return "\r";
    }

    if(_conf_helper_is_sequence(token, "\\r")) {
        return "\r";
    }

    if(_conf_helper_is_sequence(token, "\\\\")) {
        return "\\";
    }

    if(_conf_helper_is_sequence(token, "\\\"")) {
        return "\"";
    }

    return NULL;
}

void conf_helper_unescape_string(char *string) {
    char *curr;
    const char *unescaped;
    size_t string_len;

    string_len = strlen(string);

    for(curr = string; (*curr) != '\0'; curr++) {
        unescaped = _conf_helper_unescape_token(curr);
        if(unescaped != NULL) {
            memmove((curr + 1), (curr + 2), (string_len - ((curr + 1) - string)));
            memmove(curr, unescaped, 1);
            string_len -= 1;
        }
    }
}
