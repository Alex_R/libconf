#ifndef CONF_BLOCK_H_INCLUDED
#define CONF_BLOCK_H_INCLUDED

#include "conf.h"
#include "conf_value.h"


/* conf_block
 * Encapsulates a conf block.
 */
struct conf_block {
    char *name;

    struct conf_value **values;
    int values_len;

    struct conf_block **blocks;
    int blocks_len;
};


/* conf_block_create
 * Allocate and initialise a new conf_block structure representing the supplied block name and data.
 * Note: This function may call itself recursively.
 *
 * @ struct conf *conf      - The parent conf.
 * @ const char *block_name - The name of the new conf block.
 * @ const char *block_data - The data to initialise the new conf block with.
 *
 * # struct conf_block * - A newly allocated conf_block structure representing the data, or NULL on error
 *                         with conf->error set appropriately.
 */
struct conf_block *conf_block_create(struct conf *conf, const char *block_name, const char *block_data);

/* conf_block_destroy
 * Destroy the supplied conf_block structure.
 * Note: This function may call itself recursively.
 *
 * @ struct conf_block *block - The block to destroy.
 */
void conf_block_destroy(struct conf_block *block);

#endif
