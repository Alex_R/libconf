#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "conf_block.h"
#include "conf.h"
#include "conf_value.h"
#include "conf_helper.h"
#include "conf_error.h"


static const char *_conf_block_find_end(const char *block_data) {
    /* _conf_block_find_end
     * Find the end of the supplied block data.
     * The end is either a null terminator, or an unpaired closing brace.
     *
     * @ const char *block_data - The block data to find the end of.
     *
     * @ const char * - A pointer to the end of the block data.
     */

    const char *block_end;
    bool in_string = false;
    int braces = 0;

    for(block_end = block_data; (*block_end) != '\0'; block_end++) {
        conf_helper_update_in_string(&in_string, block_end, block_data);

        if(!in_string && conf_helper_is_block_start(*block_end)) {
            braces++;
            continue;
        }

        if(!in_string && conf_helper_is_block_end(*block_end)) {
            if(braces != 0) {
                braces--;
            } else {
                return block_end;
            }
        }
    }

    return block_end;
}


static char **_conf_block_tokenise(struct conf *conf, const char *block_data, const char *block_end) {
    /* _conf_block_tokenise
     * Split the supplied block data into tokens.
     * The returned token list is terminated with a NULL pointer.
     *
     * @ struct conf *conf      - The parent conf.
     * @ const char *block_data - The block data to tokenise.
     * @ const char *block_end  - The end of the block data.
     *
     * @ char ** - A newly allocated list of the tokens, or NULL on error with conf->error set appropriately.
     */

    const char *curr, *token_end;
    int in_block = 0;
    char **tokens = NULL;
    int token_count = 0;

    for(curr = block_data; curr < block_end; curr++) {
        for(token_end = curr; token_end < block_end; token_end++) {
            if(conf_helper_is_block_start(*token_end)) {
                in_block++;
            }

            if(conf_helper_is_block_end(*token_end)) {
                if(in_block != 0) {
                    in_block--;
                } else {
                    break;
                }
            }

            if(conf_helper_is_token_end(*token_end) && !in_block) {
                break;
            }
        }

        token_count++;
        tokens = realloc(tokens, (sizeof(char *) * token_count));
        if(tokens == NULL) {
            CONF_ERROR_SET(conf, "_conf_block_tokenise() failed: realloc() failed: %s", strerror(errno));
            return NULL;
        }

        tokens[token_count - 1] = malloc(sizeof(char) * ((token_end - curr) + 1));
        if(tokens[token_count - 1] == NULL) {
            CONF_ERROR_SET(conf, "_conf_block_tokenise() failed: malloc() failed: %s", strerror(errno));
            return NULL;
        }
        memset(tokens[token_count - 1], '\0', (sizeof(char) * ((token_end - curr) + 1)));

        memcpy(tokens[token_count - 1], curr, (token_end - curr));

        curr = token_end;
    }

    /* Add null sentinel. */
    token_count++;
    tokens = realloc(tokens, (sizeof(char *) * token_count));
    if(tokens == NULL) {
        CONF_ERROR_SET(conf, "_conf_block_tokenise() failed: realloc() failed: %s", strerror(errno));
        return NULL;
    }
    tokens[token_count - 1] = NULL;

    return tokens;
}


int _conf_block_parse_token(struct conf *conf, struct conf_block *block, char *token) {
    /* _conf_block_parse_token
     * Parse the supplied token, storing the results in the supplied block.
     * The token may be edited in place.
     *
     * @ struct conf *conf        - The parent conf.
     * @ struct conf_block *block - The block to store the result of the parsing in.
     * @ char *token              - The token to parse.
     *
     * int - 0 on success, or -1 on error with conf->error set appropriately.
     */

    char *seperator, *name, *value;
    struct conf_block *new_block;
    struct conf_value *new_value;

    for(seperator = token; (*seperator) != '\0'; seperator++) {
        if(conf_helper_is_token_seperator(*seperator)) {
            break;
        }
    }

    if(*seperator == '\0') {
        CONF_ERROR_SET(conf, "Missing seperator while splitting token '%s'", token);
        return -1;
    }

    *seperator = '\0';
    name = token;
    value = seperator+1;

    if(conf_get_block(conf, block, name) != NULL) {
        CONF_ERROR_SET(conf, "Redefinition of block '%s'", name);
        return -1;
    }

    if(conf_get_value(conf, block, name) != NULL) {
        CONF_ERROR_SET(conf, "Redefinition of value '%s'", name);
        return -1;
    }

    if(conf_helper_is_block_start(value[0])) {
        new_block = conf_block_create(conf, name, &value[1]);
        if(new_block == NULL) {
            return -1;
        }

        block->blocks_len++;
        block->blocks = realloc(block->blocks, (sizeof(struct conf_block *) * block->blocks_len));
        if(block->blocks == NULL) {
            CONF_ERROR_SET(conf, "_conf_block_parse_token() failed: realloc() failed: %s", strerror(errno));
            return -1;
        }

        block->blocks[block->blocks_len - 1] = new_block;
    } else {
        new_value = conf_value_create(conf, name, value);
        if(new_value == NULL) {
            return -1;
        }

        block->values_len++;
        block->values = realloc(block->values, (sizeof(struct conf_value *) * block->values_len));
        if(block->values == NULL) {
            CONF_ERROR_SET(conf, "_conf_block_parse_token() failed: realloc() failed: %s", strerror(errno));
            return -1;
        }

        block->values[block->values_len - 1] = new_value;
    }

    return 0;
}


struct conf_block *conf_block_create(struct conf *conf, const char *block_name, const char *block_data) {
    struct conf_block *block;
    const char *block_end;
    char **tokens;
    int iter;

    block = malloc(sizeof(struct conf_block));
    if(block == NULL) {
        CONF_ERROR_SET(conf, "conf_block_create() failed: malloc() failed: %s", strerror(errno));
        return NULL;
    }
    memset(block, 0, sizeof(struct conf_block));

    block->name = strdup(block_name);
    if(block->name == NULL) {
        CONF_ERROR_SET(conf, "conf_block_create() failed: strdup() failed: %s", strerror(errno));
        free(block);
        return NULL;
    }

    block_end = _conf_block_find_end(block_data);

    tokens = _conf_block_tokenise(conf, block_data, block_end);
    if(tokens == NULL) {
        free(block);
        return NULL;
    }

    for(iter = 0; tokens[iter] != NULL; iter++) {
        if(_conf_block_parse_token(conf, block, tokens[iter]) == -1) {
            free(block);
            return NULL;
        }
        free(tokens[iter]);
    }
    free(tokens);

    return block;
}

void conf_block_destroy(struct conf_block *block) {
    int iter;

    for(iter = 0; iter < block->values_len; iter++) {
        conf_value_destroy(block->values[iter]);
    }
    free(block->values);

    for(iter = 0; iter < block->blocks_len; iter++) {
        conf_block_destroy(block->blocks[iter]);
    }
    free(block->blocks);

    free(block->name);
    free(block);
}
