#ifndef CONF_FILE_H_INCLUDED
#define CONF_FILE_H_INCLUDED

#include "conf.h"


/* conf_file
 * Encapsulates a conf file.
 */
struct conf_file {
    char *name;
    struct conf_block *root;
};


/* conf_file_create
 * Allocate and initialise a new conf_file structure representing the file at the supplied path.
 *
 * @ struct conf *conf     - The parent conf.
 * @ const char *file_path - The path to the file to load.
 *
 * # struct conf_file * - A newly allocated conf_file structure representing the loaded file, or NULL on error with
 *                        conf->error set appropriately.
 */
struct conf_file *conf_file_create(struct conf *conf, const char *file_path);

/* conf_file_destroy
 * Destroy the supplied conf_file structure.
 *
 * @ struct conf_file *file - The file to destroy.
 */
void conf_file_destroy(struct conf_file *file);

#endif
