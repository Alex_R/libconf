#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

#include "conf_file.h"
#include "conf.h"
#include "conf_block.h"
#include "conf_helper.h"
#include "conf_error.h"


static char *_conf_file_load_raw_data(struct conf *conf, const char *file_path) {
    /* _conf_file_load_raw_data
     * Load the raw data from the file at the supplied path.
     *
     * @ struct conf *conf     - The parent conf.
     * @ const char *file_path - The path to the file to load.
     *
     * # char * - A newly allocated buffer containing the loaded data, or NULL on error with conf->error set
     *            appropriately.
     */

    FILE *fp;
    long data_size;
    char *data;

    fp = fopen(file_path, "r");
    if(fp == NULL) {
        CONF_ERROR_SET(conf, "_conf_file_load_raw_data(%s) failed: fopen() failed: %s", file_path, strerror(errno));
        return NULL;
    }

    if(fseek(fp, 0, SEEK_END) == -1) {
        CONF_ERROR_SET(conf, "_conf_file_load_raw_data(%s) failed: fseek() failed: %s", file_path, strerror(errno));
        return NULL;
    }

    data_size = ftell(fp);
    if(data_size == -1) {
        CONF_ERROR_SET(conf, "_conf_file_load_raw_data(%s) failed: ftell() failed: %s", file_path, strerror(errno));
        return NULL;
    }

    if(fseek(fp, 0, SEEK_SET) == -1) {
        CONF_ERROR_SET(conf, "_conf_file_load_raw_data(%s) failed: fseek() failed: %s", file_path, strerror(errno));
        return NULL;
    }

    data = malloc(sizeof(char) * (data_size + 1));
    if(data == NULL) {
        CONF_ERROR_SET(conf, "_conf_file_load_raw_data(%s) failed: malloc() failed: %s", file_path, strerror(errno));
        fclose(fp);
        return NULL;
    }
    memset(data, 0, (sizeof(char) * (data_size + 1)));

    if(fread(data, data_size, 1, fp) != 1) {
        CONF_ERROR_SET(conf, "_conf_file_load_raw_data(%s) failed: fread() failed: %s", file_path, strerror(errno));
        free(data);
        fclose(fp);
        return NULL;
    }

    if(fclose(fp) != 0) {
        CONF_ERROR_SET(conf, "_conf_file_load_raw_data(%s) failed: fclose() failed: %s", file_path, strerror(errno));
        free(data);
        return NULL;
    }

    return data;
}


static void _conf_file_preprocess_remove_comments_oneline(char *file_data, size_t *file_data_len) {
    /* _conf_file_preprocess_remove_comments_oneline
     * Remove oneline comments in the supplied file data.  The data and size are modified in place.
     *
     * @ char *file_data       - The data to remove the oneline comments in.
     * @ size_t *file_data_len - The size of the data.
     */

    char *curr, *token_end;
    bool in_string = false;

    for(curr = file_data; (*curr) != '\0'; curr++) {
        conf_helper_update_in_string(&in_string, curr, file_data);

        if(conf_helper_is_oneline_comment_start(curr) && !in_string) {
            for(token_end = curr; (*token_end) != '\0'; token_end++) {
                if(conf_helper_is_newline(*token_end)) {
                    break;
                }
            }

            memmove(curr, token_end, ((*file_data_len - (token_end - file_data)) + 1));

            *file_data_len -= (token_end - curr);

            curr--;
        }
    }
}

static void _conf_file_preprocess_remove_comments_multiline(char *file_data, size_t *file_data_len) {
    /* _conf_file_preprocess_remove_comments_multiline
     * Remove multiline comments in the supplied file data.  The data and size are modified in place.
     *
     * @ char *file_data       - The data to remove the multiline comments in.
     * @ size_t *file_data_len - The size of the data.
     */

    char *curr, *token_end;
    bool in_string = false;

    for(curr = file_data; (*curr) != '\0'; curr++) {
        conf_helper_update_in_string(&in_string, curr, file_data);

        if(conf_helper_is_multiline_comment_start(curr) && !in_string) {
            for(token_end = curr; (*token_end) != '\0'; token_end++) {
                if(conf_helper_is_multiline_comment_end(token_end)) {
                    break;
                }
            }

            if(*token_end != '\0') {
                token_end += 2;
            }

            memmove(curr, token_end, ((*file_data_len - (token_end - file_data)) + 1));

            *file_data_len -= (token_end - curr);

            curr--;
        }
    }
}


static void _conf_file_preprocess_remove_comments(char *file_data, size_t *file_data_len) {
    /* _conf_file_preprocess_remove_comments
     * Remove comments in the supplied file data.  The data and size are modified in place.
     *
     * @ char *file_data       - The data to remove the comments in.
     * @ size_t *file_data_len - The size of the data.
     */

    _conf_file_preprocess_remove_comments_oneline(file_data, file_data_len);
    _conf_file_preprocess_remove_comments_multiline(file_data, file_data_len);
}

static void _conf_file_preprocess_remove_whitespace(char *file_data, size_t *file_data_len) {
    /* _conf_file_preprocess_remove_whitespace
     * Remove whitespace in the supplied file data.  The data and size are modified in place.
     *
     * @ char *file_data       - The data to remove the whitespace in.
     * @ size_t *file_data_len - The size of the data.
     */

    char *curr, *token_end;
    bool in_string = false;

    for(curr = file_data; (*curr) != '\0'; curr++) {
        conf_helper_update_in_string(&in_string, curr, file_data);

        if(conf_helper_is_whitespace(*curr) && !in_string) {
            /* Find the end of the whitespace token. */
            for(token_end = curr; (*token_end) != '\0'; token_end++) {
                if(!conf_helper_is_whitespace(*token_end)) {
                    break;
                }
            }

            /* Fold from the end of the token to the start. */
            memmove(curr, token_end, ((*file_data_len - (token_end - file_data)) + 1));

            *file_data_len -= (token_end - curr);

            curr--;
        }
    }
}

static void _conf_file_preprocess_remove_newlines(char *file_data, size_t *file_data_len) {
    /* _conf_file_preprocess_remove_newlines
     * Remove newlines in the supplied file data.  The data and size are modified in place.
     *
     * @ char *file_data       - The data to remove the newlines in.
     * @ size_t *file_data_len - The size of the data.
     */

    char *curr, *token_end;
    bool in_string = false;

    for(curr = file_data; (*curr) != '\0'; curr++) {
        conf_helper_update_in_string(&in_string, curr, file_data);

        if(conf_helper_is_newline(*curr) && !in_string) {
            /* Find the end of the newline token. */
            for(token_end = curr; (*token_end) != '\0'; token_end++) {
                if(!conf_helper_is_newline(*token_end)) {
                    break;
                }
            }

            /* Fold from the end of the token to the start. */
            memmove(curr, token_end, ((*file_data_len - (token_end - file_data)) + 1));

            *file_data_len -= (token_end - curr);

            curr--;
        }
    }
}

static char *_conf_file_preprocess(struct conf *conf, const char *raw_file_data) {
    /* _conf_file_preprocess
     * Preprocess the supplied raw file data.
     * The general aim here is to generate a uniform "data-only" version of the file data, removing
     * all whitespace, comments etc.  This simplifies later parsing.
     *
     * @ struct conf *conf         - The parent conf.
     * @ const char *raw_file_data - The raw data to preprocess.
     *
     * # char * - The preprocessed data, or NULL on error with conf->error set appropriately.
     */

    size_t file_data_len;
    char *file_data;

    file_data_len = strlen(raw_file_data);

    file_data = malloc(sizeof(char) * (file_data_len + 1));
    if(file_data == NULL) {
        CONF_ERROR_SET(conf, "_conf_file_preprocess() failed: malloc() failed: %s", strerror(errno));
        return NULL;
    }

    memcpy(file_data, raw_file_data, (sizeof(char) * (file_data_len + 1)));

    _conf_file_preprocess_remove_comments(file_data, &file_data_len);
    _conf_file_preprocess_remove_whitespace(file_data, &file_data_len);
    _conf_file_preprocess_remove_newlines(file_data, &file_data_len);

    return file_data;
}


struct conf_file *conf_file_create(struct conf *conf, const char *file_path) {
    struct conf_file *file;
    char *raw_file_data, *file_data;

    file = malloc(sizeof(struct conf_file));
    if(file == NULL) {
        CONF_ERROR_SET(conf, "conf_file_create(%s) failed: malloc(): %s", file_path, strerror(errno));
        return NULL;
    }
    memset(file, 0, sizeof(struct conf_file));

    raw_file_data = _conf_file_load_raw_data(conf, file_path);
    if(raw_file_data == NULL) {
        free(file);
        return NULL;
    }

    file_data = _conf_file_preprocess(conf, raw_file_data);
    if(file_data == NULL) {
        free(raw_file_data);
        free(file);
        return NULL;
    }

    free(raw_file_data);

    file->root = conf_block_create(conf, "root", file_data);
    if(file->root == NULL) {
        free(file_data);
        free(file);
        return NULL;
    }

    free(file_data);

    return file;
}

void conf_file_destroy(struct conf_file *file) {
    conf_block_destroy(file->root);

    free(file);
}
