#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "conf.h"
#include "conf_file.h"
#include "conf_block.h"
#include "conf_value.h"
#include "conf_error.h"


struct conf *conf_create(void) {
    struct conf *conf;

    conf = malloc(sizeof(struct conf));
    if(conf == NULL) {
        return NULL;
    }
    memset(conf, 0, sizeof(struct conf));

    return conf;
}

void conf_destroy(struct conf *conf) {
    if(conf->file != NULL) {
        conf_file_destroy(conf->file);
    }

    if(conf->error != NULL) {
        conf_error_destroy(conf->error);
    }

    free(conf);
}


const char *conf_get_error(struct conf *conf) {
    if(conf->error == NULL) {
        return NULL;
    }

    return conf->error->value;
}


int conf_load_file(struct conf *conf, const char *file_path) {
    conf->file = conf_file_create(conf, file_path);
    if(conf->file == NULL) {
        return -1;
    }

    return 0;
}


struct conf_block *conf_get_block(struct conf *conf, struct conf_block *block, const char *name) {
    int iter;

    if(block == NULL) {
        block = conf->file->root;
    }

    for(iter = 0; iter < block->blocks_len; iter++) {
        if(strlen(block->blocks[iter]->name) == strlen(name)) {
            if(strncmp(block->blocks[iter]->name, name, strlen(name)) == 0) {
                return block->blocks[iter];
            }
        }
    }

    return NULL;
}

struct conf_value *conf_get_value(struct conf *conf, struct conf_block *block, const char *name) {
    int iter;

    if(block == NULL) {
        block = conf->file->root;
    }

    for(iter = 0; iter < block->values_len; iter++) {
        if(strlen(block->values[iter]->name) == strlen(name)) {
            if(strncmp(block->values[iter]->name, name, strlen(name)) == 0) {
                return block->values[iter];
            }
        }
    }

    return NULL;
}


char *conf_get_value_str(struct conf_value *value) {
    if(value->type != CONF_VALUE_TYPE_STR) {
        return NULL;
    }

    return value->v_str;
}

conf_int_t *conf_get_value_int(struct conf_value *value) {
    if(value->type != CONF_VALUE_TYPE_INT) {
        return NULL;
    }

    return &value->v_int;
}

bool *conf_get_value_bool(struct conf_value *value) {
    if(value->type != CONF_VALUE_TYPE_BOOL) {
        return NULL;
    }

    return &value->v_bool;
}


char *conf_get_value_str_or(struct conf *conf, struct conf_block *block, const char *name, char *def) {
    struct conf_value *value;
    char *v_str;

    value = conf_get_value(conf, block, name);
    if(value == NULL) {
        return def;
    }

    v_str = conf_get_value_str(value);
    if(v_str == NULL) {
        return def;
    }

    return v_str;
}

conf_int_t conf_get_value_int_or(struct conf *conf, struct conf_block *block, const char *name, conf_int_t def) {
    struct conf_value *value;
    conf_int_t *v_int;

    value = conf_get_value(conf, block, name);
    if(value == NULL) {
        return def;
    }

    v_int = conf_get_value_int(value);
    if(v_int == NULL) {
        return def;
    }

    return *v_int;
}

bool conf_get_value_bool_or(struct conf *conf, struct conf_block *block, const char *name, bool def) {
    struct conf_value *value;
    bool *v_bool;

    value = conf_get_value(conf, block, name);
    if(value == NULL) {
        return def;
    }

    v_bool = conf_get_value_bool(value);
    if(v_bool == NULL) {
        return def;
    }

    return *v_bool;
}


int conf_grab_set_error_callback(struct conf *conf, conf_grab_error_callback_t *error_callback) {
    conf->grab_error_callback = error_callback;
    return 0;
}

char **_conf_grab_resolve_path_split(const char *path) {
    /* _conf_grab_resolve_path_split
     * Split the supplied path into a list of path parts.
     *
     * @ const char *path - The path to split.
     *
     * @ char ** - The list of path parts, terminated by a NULL sentinel, or NULL on error with errno set appropriately..
     */

    char *path_dup, *part, *save_ptr, **parts = NULL;
    int parts_len = 0;

    parts = realloc(parts, ((sizeof(char *) * (parts_len + 1))));
    if(parts == NULL) {
        goto error;
    }
    parts[parts_len] = 0;

    path_dup = strdup(path);
    if(path_dup == NULL) {
        goto free_parts_and_error;
    }

    part = strtok_r(path_dup, ".", &save_ptr);

    while(part != NULL) {
        parts_len++;
        parts = realloc(parts, ((sizeof(char *) * (parts_len + 1))));
        if(parts == NULL) {
            goto free_path_and_parts_and_error;
        }

        parts[parts_len - 1] = strdup(part);
        if(parts[parts_len - 1] == NULL) {
            goto free_path_and_parts_and_error;
        }
        parts[parts_len] = NULL;

        part = strtok_r(NULL, ".", &save_ptr);
    }

    free(path_dup);

    return parts;

    free_path_and_parts_and_error:
        free(path_dup);
    free_parts_and_error:
        free(parts);
    error:
        return NULL;
}

void _conf_grab_resolve_path_split_free(char **parts) {
    int iter;
    char *part;

    for(iter = 0, part = parts[0]; part != NULL; iter++, part = parts[iter]) {
        free(part);
    }

    free(parts);
}

struct conf_block *_conf_grab_resolve_path_block(struct conf *conf, struct conf_block *start, const char *path) {
    /* _conf_grab_resolve_path_block
     * Resolve the supplied path into a conf_block structure.
     * The path is a series of dot delimited block names, then the desired block name.
     *
     * @ struct conf *conf        - The parent conf.
     * @ struct conf_block *start - The block to start resolution on.
     * @ const char *path         - The path to resolve.
     *
     * # struct conf_block * - The block described by the path, or NULL if it doesn't exist.
     */

    char **parts, *part;
    int iter;
    struct conf_block *block = start;
    struct conf_block *dest_block;

    parts = _conf_grab_resolve_path_split(path);
    if(parts == NULL) {
        return NULL;
    }

    for(iter = 0, part = parts[0]; part != NULL; iter++, part = parts[iter]) {
        if(parts[iter + 1] == NULL) {
            break;
        }

        block = conf_get_block(conf, block, part);
        if(block == NULL) {
            _conf_grab_resolve_path_split_free(parts);
            return NULL;
        }
    }

    dest_block = conf_get_block(conf, block, part);

    _conf_grab_resolve_path_split_free(parts);
    return dest_block;
}

struct conf_value *_conf_grab_resolve_path_value(struct conf *conf, struct conf_block *start, const char *path) {
    /* _conf_grab_resolve_path_value
     * Resolve the supplied path into a conf_value structure.
     * The path is a series of dot delimited block names, then the value name.
     *
     * @ struct conf *conf        - The parent conf.
     * @ struct conf_block *start - The block to start resolution on.
     * @ const char *path         - The path to resolve.
     *
     * # struct conf_value * - The value described by the path, or NULL if it doesn't exist.
     */

    char **parts, *part;
    int iter;
    struct conf_block *block = start;
    struct conf_value *value;

    parts = _conf_grab_resolve_path_split(path);
    if(parts == NULL) {
        return NULL;
    }

    for(iter = 0, part = parts[0]; part != NULL; iter++, part = parts[iter]) {
        if(parts[iter + 1] == NULL) {
            break;
        }

        block = conf_get_block(conf, block, part);
        if(block == NULL) {
            _conf_grab_resolve_path_split_free(parts);
            return NULL;
        }
    }

    value = conf_get_value(conf, block, part);

    _conf_grab_resolve_path_split_free(parts);
    return value;
}

void conf_grab_value_str_rel(struct conf *conf, struct conf_block *root, char **dest, char *path, char *def) {
    struct conf_value *value;
    char *str_value, *dup_str;

    value = _conf_grab_resolve_path_value(conf, root, path);
    if(value == NULL) {
        goto do_default;
    }

    str_value = conf_get_value_str(value);
    if(str_value == NULL) {
        goto do_default;
    }

    dup_str = strdup(str_value);
    if(dup_str == NULL) {
        if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_value_str() failed: strdup() failed"); }
        return;
    }

    *dest = dup_str;
    return;

    do_default:
        dup_str = strdup(def);
        if(dup_str == NULL) {
            if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_value_str() failed: strdup() failed"); }
            return;
        }

        *dest = dup_str;
        return;
}

void conf_grab_value_int_rel(struct conf *conf, struct conf_block *root, conf_int_t *dest, char *path, conf_int_t def) {
    struct conf_value *value;
    conf_int_t *int_value;

    value = _conf_grab_resolve_path_value(conf, root, path);
    if(value == NULL) {
        goto do_default;
    }

    int_value = conf_get_value_int(value);
    if(int_value == NULL) {
        goto do_default;
    }

    *dest = *int_value;
    return;

    do_default:
        *dest = def;
        return;
}

void conf_grab_value_bool_rel(struct conf *conf, struct conf_block *root, bool *dest, char *path, bool def) {
    struct conf_value *value;
    bool *bool_value;

    value = _conf_grab_resolve_path_value(conf, root, path);
    if(value == NULL) {
        goto do_default;
    }

    bool_value = conf_get_value_bool(value);
    if(bool_value == NULL) {
        goto do_default;
    }

    *dest = *bool_value;
    return;

    do_default:
        *dest = def;
        return;
}

void conf_grab_block_rel(struct conf *conf, struct conf_block *root, struct conf_block **dest, char *path, struct conf_block *def) {
    struct conf_block *block;

    block = _conf_grab_resolve_path_block(conf, root, path);
    if(block == NULL) {
        goto do_default;
    }

    *dest = block;
    return;

    do_default:
        *dest = def;
        return;
}

void conf_grab_value_str(struct conf *conf, char **dest, char *path, char *def) {
    return conf_grab_value_str_rel(conf, NULL, dest, path, def);
}

void conf_grab_value_int(struct conf *conf, conf_int_t *dest, char *path, conf_int_t def) {
    return conf_grab_value_int_rel(conf, NULL, dest, path, def);
}

void conf_grab_value_bool(struct conf *conf, bool *dest, char *path, bool def) {
    return conf_grab_value_bool_rel(conf, NULL, dest, path, def);
}

void conf_grab_block(struct conf *conf, struct conf_block **dest, char *path, struct conf_block *def) {
    return conf_grab_block_rel(conf, NULL, dest, path, def);
}

void conf_grab_value_str_rel_nodef(struct conf *conf, struct conf_block *root, char **dest, char *path) {
    struct conf_value *value;
    char *str_value, *dup_str;

    value = _conf_grab_resolve_path_value(conf, root, path);
    if(value == NULL) {
        if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_value_str_nodef() failed: Value does not exist"); }
        return;
    }

    str_value = conf_get_value_str(value);
    if(str_value == NULL) {
        if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_value_str_nodef() failed: Value is not a string"); }
        return;
    }

    dup_str = strdup(str_value);
    if(dup_str == NULL) {
        if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_value_str_nodef() failed: strdup() failed"); }
        return;
    }

    *dest = dup_str;
    return;
}

void conf_grab_value_int_rel_nodef(struct conf *conf, struct conf_block *root, conf_int_t *dest, char *path) {
    struct conf_value *value;
    conf_int_t *int_value;

    value = _conf_grab_resolve_path_value(conf, root, path);
    if(value == NULL) {
        if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_value_int_nodef() failed: Value does not exist"); }
        return;
    }

    int_value = conf_get_value_int(value);
    if(int_value == NULL) {
        if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_value_int_nodef() failed: Value is not an int"); }
        return;
    }

    *dest = *int_value;
    return;
}

void conf_grab_value_bool_rel_nodef(struct conf *conf, struct conf_block *root, bool *dest, char *path) {
    struct conf_value *value;
    bool *bool_value;

    value = _conf_grab_resolve_path_value(conf, root, path);
    if(value == NULL) {
        if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_value_bool_nodef() failed: Value does not exist"); }
        return;
    }

    bool_value = conf_get_value_bool(value);
    if(bool_value == NULL) {
        if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_value_bool_nodef() failed: Value is not an bool"); }
        return;
    }

    *dest = *bool_value;
    return;
}

void conf_grab_block_rel_nodef(struct conf *conf, struct conf_block *root, struct conf_block **dest, char *path) {
    struct conf_block *block;

    block = _conf_grab_resolve_path_block(conf, root, path);
    if(block == NULL) {
        if(conf->grab_error_callback != NULL) { conf->grab_error_callback(conf, "conf_grab_block_nodef() failed: Block does not exist"); }
        return;
    }

    *dest = block;
    return;
}

void conf_grab_value_str_nodef(struct conf *conf, char **dest, char *path) {
    return conf_grab_value_str_rel_nodef(conf, NULL, dest, path);
}

void conf_grab_value_int_nodef(struct conf *conf, conf_int_t *dest, char *path) {
    return conf_grab_value_int_rel_nodef(conf, NULL, dest, path);
}

void conf_grab_value_bool_nodef(struct conf *conf, bool *dest, char *path) {
    return conf_grab_value_bool_rel_nodef(conf, NULL, dest, path);
}

void conf_grab_block_nodef(struct conf *conf, struct conf_block **dest, char *path) {
    return conf_grab_block_rel_nodef(conf, NULL, dest, path);
}


const char *conf_get_bool_str(bool v_bool) {
    return v_bool ? "true" : "false";
}


conf_int_t conf_map_str_to_int(const char *str, struct conf_str_int_map *map, conf_int_t def) {
    struct conf_str_int_map *mapping;

    if(str == NULL) {
        goto do_default;
    }

    for(mapping = map; mapping->key != NULL; mapping++) {
        if(strlen(mapping->key) == strlen(str)) {
            if(strncmp(mapping->key, str, strlen(str)) == 0) {
                return mapping->value;
            }
        }
    }

    do_default:
        return def;
}


char *conf_get_block_name(struct conf_block *block) {
    return block->name;
}


int conf_get_value_type(struct conf_value *value) {
    return value->type;
}

char *conf_get_value_name(struct conf_value *value) {
    return value->name;
}


struct conf_iter *conf_iter_create(struct conf *conf, struct conf_block *block) {
    struct conf_iter *iter;

    if(block == NULL) {
        block = conf->file->root;
    }

    iter = malloc(sizeof(struct conf_iter));
    if(iter == NULL) {
        CONF_ERROR_SET(conf, "conf_iter_create() failed: malloc() failed: %s", strerror(errno));
        return NULL;
    }

    memset(iter, 0, sizeof(struct conf_iter));
    iter->block = block;

    return iter;
}

void conf_iter_destroy(struct conf_iter *iter) {
    free(iter);
}

struct conf_block *conf_iter_next_block(struct conf_iter *iter) {
    if(iter->block_index < iter->block->blocks_len) {
        return iter->block->blocks[(iter->block_index++)];
    }

    return NULL;
}

struct conf_value *conf_iter_next_value(struct conf_iter *iter) {
    if(iter->value_index < iter->block->values_len) {
        return iter->block->values[(iter->value_index++)];
    }

    return NULL;
}
