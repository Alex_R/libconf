#ifndef CONF_VALUE_H_INCLUDED
#define CONF_VALUE_H_INCLUDED

#include <stdint.h>
#include <stdbool.h>

#include "conf.h"


#define CONF_VALUE_TYPE_STR  0x1
#define CONF_VALUE_TYPE_INT  0x2
#define CONF_VALUE_TYPE_BOOL 0x3


/* conf_value
 * Encapsulates a conf value.
 */
struct conf_value {
    char *name;
    int type;

    union {
        char *v_str;
        conf_int_t v_int;
        bool v_bool;
    };
};


/* conf_value_create
 * Allocate and initialise a new conf_value structure representing the supplied value name and data.
 *
 * @ struct conf *conf      - The parent conf.
 * @ const char *value_name - The name of the new conf value.
 * @ const char *value_data - The data to initialise the new conf value with.
 *
 * # struct conf_value * - A newly allocated conf_value structure representing the data, or NULL on error
 *                         with conf->error set appropriately.
 */
struct conf_value *conf_value_create(struct conf *conf, const char *value_name, const char *value_data);

/* conf_value_destroy
 * Destroy the supplied conf_value structure.
 *
 * @ struct conf_value *value - The value to destroy.
 */
void conf_value_destroy(struct conf_value *value);

#endif
