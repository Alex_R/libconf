LIBDIR=/usr/lib64

all:
	mkdir -p ./build/
	mkdir -p ./build/lib/
	mkdir -p ./build/include/
	mkdir -p ./build/examples/

	clang -g -c -fpic ./src/conf.c ./src/conf_file.c ./src/conf_block.c ./src/conf_value.c ./src/conf_helper.c ./src/conf_error.c
	clang -g -shared -o ./build/lib/libconf.so ./conf.o ./conf_file.o ./conf_block.o ./conf_value.o ./conf_helper.o ./conf_error.o
	rm ./conf.o ./conf_file.o ./conf_block.o ./conf_value.o ./conf_helper.o ./conf_error.o

	cp ./src/conf.h ./build/include/
	cp ./src/conf_file.h ./build/include/
	cp ./src/conf_block.h ./build/include/
	cp ./src/conf_value.h ./build/include/
	cp ./src/conf_helper.h ./build/include/
	cp ./src/conf_error.h ./build/include/

	clang -g -o ./build/examples/example1 ./src/examples/example1.c \
	-I ./build/include -L ./build/lib/ -lconf
	clang -g -o ./build/examples/example2 ./src/examples/example2.c \
	-I ./build/include -L ./build/lib/ -lconf
	clang -g -o ./build/examples/example3 ./src/examples/example3.c \
	-I ./build/include -L ./build/lib/ -lconf
	clang -g -o ./build/examples/example4 ./src/examples/example4.c \
	-I ./build/include -L ./build/lib/ -lconf
	clang -g -o ./build/examples/example5 ./src/examples/example5.c \
	-I ./build/include -L ./build/lib/ -lconf
	clang -g -o ./build/examples/example6 ./src/examples/example6.c \
	-I ./build/include -L ./build/lib/ -lconf
	clang -g -o ./build/examples/example7 ./src/examples/example7.c \
	-I ./build/include -L ./build/lib/ -lconf

install:
	install -D -m 0755 ./build/lib/libconf.so ${DESTDIR}/${LIBDIR}/libconf.so
	install -D -m 0644 ./build/include/conf.h ${DESTDIR}/usr/include/conf.h
	install -D -m 0644 ./build/include/conf_file.h ${DESTDIR}/usr/include/conf_file.h
	install -D -m 0644 ./build/include/conf_block.h ${DESTDIR}/usr/include/conf_block.h
	install -D -m 0644 ./build/include/conf_value.h ${DESTDIR}/usr/include/conf_value.h
	install -D -m 0644 ./build/include/conf_helper.h ${DESTDIR}/usr/include/conf_helper.h
	install -D -m 0644 ./build/include/conf_error.h ${DESTDIR}/usr/include/conf_error.h
