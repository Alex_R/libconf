libconf - A library for doing things with configs.


See src/conf.h for API documentation.

See src/examples/example1.c for basic usage.


Installation:
```
make
make install
```